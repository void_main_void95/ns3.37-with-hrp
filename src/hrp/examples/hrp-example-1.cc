/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */

#include "ns3/hrp-module.h"
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/wifi-module.h"
#include "ns3/v4ping-helper.h"
#include "ns3/udp-echo-server.h"
#include "ns3/udp-echo-client.h"
#include "ns3/udp-echo-helper.h"
#include <iostream>

using namespace ns3;


class HrpExample
{
public:
  HrpExample ();
  /// Configure script parameters, \return true on successful configuration
  bool Configure (int argc, char **argv);
  /// Run simulation
  void Run ();
  /// Report results
  void Report (std::ostream & os);

private:
  ///\name parameters
  //\{
  /// Simulation time, seconds
  double totalTime;
  /// Write per-device PCAP traces if true
  bool pcap;
  //\}

  ///\name network
  //\{
  NodeContainer nodes;
  NetDeviceContainer devices;
  Ipv4InterfaceContainer interfaces;
  //\}

private:
  void CreateNodes ();
  void CreateDevices ();
  void InstallInternetStack ();
  void InstallApplications ();
};

int main (int argc, char **argv)
{
  HrpExample test;
  if (! test.Configure(argc, argv))
    NS_FATAL_ERROR ("Configuration failed. Aborted.");

  test.Run ();
  test.Report (std::cout);
  return 0;
}

//-----------------------------------------------------------------------------
HrpExample::HrpExample () :
  // Simulation time
  totalTime (10),
  // Generate capture files for each node
  pcap (true)
{
}

bool
HrpExample::Configure (int argc, char **argv)
{
  // Enable HRP logs by default. Comment this if too noisy
//  LogComponentEnable("HrpRoutingProtocol", LOG_LEVEL_ALL);
  LogComponentEnable("HrpRoutingProtocol", ns3::LOG_LEVEL_INFO);
  LogComponentEnable("UdpEchoClientApplication", ns3::LOG_LEVEL_INFO);
  LogComponentEnable("UdpEchoServerApplication", ns3::LOG_LEVEL_INFO);
  SeedManager::SetSeed(12345);
  CommandLine cmd;

  cmd.AddValue ("pcap", "Write PCAP traces.", pcap);
  cmd.AddValue ("time", "Simulation time, s.", totalTime);

  cmd.Parse (argc, argv);
  return true;
}

void
HrpExample::Run ()
{
//  Config::SetDefault ("ns3::WifiRemoteStationManager::RtsCtsThreshold", UintegerValue (1)); // enable rts cts all the time.
  CreateNodes ();
  CreateDevices ();
  InstallInternetStack ();
  InstallApplications ();

  HrpHelper hrp;
  hrp.Install ();

  std::cout << "Starting simulation for " << totalTime << " s ...\n";

  Simulator::Stop (Seconds (totalTime));
  Simulator::Run ();
  Simulator::Destroy ();
}

void
HrpExample::Report (std::ostream &)
{
}

void
HrpExample::CreateNodes ()
{
  std::cout << "Creating 4 nodes" << std::endl;
  nodes.Create (4);
  // Name nodes
  for (uint32_t i = 0; i < 4; ++i)
     {
       std::ostringstream os;
       os << "node-" << i;
       Names::Add (os.str (), nodes.Get (i));
     }
  // Create static grid
  MobilityHelper mobility;
  auto positionAllocator = ListPositionAllocator();
  positionAllocator.Add (Vector({0, 0, 0}));
  positionAllocator.Add (Vector({30, 0, 0}));
  positionAllocator.Add (Vector({0, 30, 0}));
  positionAllocator.Add (Vector({40, 40, 0}));
  mobility.SetPositionAllocator (&positionAllocator);
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (nodes);
}

void
HrpExample::CreateDevices ()
{
  std::cout << "Creating devices" << std::endl;
  WifiMacHelper wifiMac;
  wifiMac.SetType ("ns3::AdhocWifiMac");
  YansWifiPhyHelper wifiPhy;
  wifiPhy.SetErrorRateModel ("ns3::NistErrorRateModel");
  YansWifiChannelHelper wifiChannel = YansWifiChannelHelper::Default ();
  wifiPhy.SetChannel (wifiChannel.Create ());
  WifiHelper wifi;
  wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager", "DataMode", StringValue ("OfdmRate6Mbps"), "RtsCtsThreshold", UintegerValue (0));
  devices = wifi.Install (wifiPhy, wifiMac, nodes);

  if (pcap)
    {
      wifiPhy.EnablePcapAll (std::string ("hrp"));
    }
}

void
HrpExample::InstallInternetStack ()
{
  std::cout << "Installing internet stack" << std::endl;
  HrpHelper hrp;
  // you can configure HRP attributes here using hrp.Set(name, value)
  InternetStackHelper stack;
  stack.SetRoutingHelper (hrp);
  stack.Install (nodes);
  Ipv4AddressHelper address;
  address.SetBase ("10.0.0.0", "255.255.0.0");
  interfaces = address.Assign (devices);
}

void
HrpExample::InstallApplications ()
{
  uint16_t port = 9;  // well-known echo port number
  uint32_t packetSize = 1024; // size of the packets being transmitted
  uint32_t maxPacketCount = 100; // number of packets to transmit
  Time interPacketInterval = Seconds (1.); // interval between packet transmissions
  auto serverNode = nodes.Get (3);
  auto clientNode = nodes.Get (0);

  std::cout << "Installing udp echo server on node " << serverNode->GetId() << std::endl;
  UdpEchoServerHelper server1 (port);
  ApplicationContainer apps = server1.Install (serverNode);
  apps.Start (Seconds (1.0));
  apps.Stop (Seconds (totalTime-0.1));

  std::cout << "Installing udp echo client on node " << clientNode->GetId() << std::endl;
  UdpEchoClientHelper client (interfaces.GetAddress (3), port);
  client.SetAttribute ("MaxPackets", UintegerValue (maxPacketCount));
  client.SetAttribute ("Interval", TimeValue (interPacketInterval));
  client.SetAttribute ("PacketSize", UintegerValue (packetSize));
  apps = client.Install (clientNode);
  apps.Start (Seconds (2.0));
  apps.Stop (Seconds (totalTime-0.1));
}

