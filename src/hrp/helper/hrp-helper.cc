#include "hrp-helper.h"
#include "ns3/hrp.h"
#include "ns3/node-list.h"
#include "ns3/names.h"
#include "ns3/ipv4-list-routing.h"
#include "ns3/node-container.h"
#include "ns3/callback.h"
#include "ns3/udp-l4-protocol.h"

namespace ns3 {

HrpHelper::HrpHelper ()
  : Ipv4RoutingHelper ()
{
  m_agentFactory.SetTypeId ("ns3::hrp::RoutingProtocol");
}

HrpHelper*
HrpHelper::Copy () const
{
  return new HrpHelper (*this);
}

Ptr<Ipv4RoutingProtocol>
HrpHelper::Create (Ptr<Node> node) const
{
  //Ptr<Ipv4L4Protocol> ipv4l4 = node->GetObject<Ipv4L4Protocol> ();
  Ptr<hrp::HrpRoutingProtocol> hrp = m_agentFactory.Create<hrp::HrpRoutingProtocol> ();
  //hrp->SetDownTarget (ipv4l4->GetDownTarget ());
  node->AggregateObject (hrp);
  return hrp;
}

void
HrpHelper::Set (std::string name, const AttributeValue &value)
{
  m_agentFactory.Set (name, value);
}


void
HrpHelper::Install () const
{
  NodeContainer c = NodeContainer::GetGlobal ();
  for (auto i = c.Begin (); i != c.End (); ++i)
    {
      Ptr<Node> node = (*i);
      Ptr<UdpL4Protocol> udp = node->GetObject<UdpL4Protocol> ();
      Ptr<hrp::HrpRoutingProtocol> hrp = node->GetObject<hrp::HrpRoutingProtocol> ();
      hrp->SetDownTarget (udp->GetDownTarget ());
    }
}

}
