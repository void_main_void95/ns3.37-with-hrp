#include "hrp-habitat.h"

namespace ns3::hrp {
Habitat::Habitat (Vector f1, Vector f2, double radius)
{
  m_f1 = f1;
  m_f2 = f2;
  m_radius = radius;
}

std::pair<Vector, Vector> Habitat::GetOrderedFoci (Vector f1, Vector f2)
{
  Vector fMax, fMin;
  if (f1.x >= f2.x){
      fMax.x = f1.x;
      fMin.x = f2.x;
    }
  else{
      fMax.x = f2.x;
      fMin.x = f1.x;
    }
  if (f1.y >= f2.y){
      fMax.y = f1.y;
      fMin.y = f2.y;
    }
  else{
      fMax.y = f2.y;
      fMin.y = f1.y;
    }
  return {fMax, fMin};
}

std::pair<Vector, Vector> Habitat::GetOrderedFoci () const
{
  return Habitat::GetOrderedFoci (m_f1, m_f1);
}

std::pair<double, double> Habitat::GetEdges (Vector f1, Vector f2, double radius)
{
  double le, re;
  auto orderedFoci = GetOrderedFoci (f1, f2);
  le = (orderedFoci.first.x + orderedFoci.second.x + orderedFoci.first.y - orderedFoci.second.y - radius) / 2;
  re = (orderedFoci.first.x + orderedFoci.second.x - orderedFoci.first.y + orderedFoci.second.y + radius) / 2;
  return {re, le};
}

std::pair<double, double> Habitat::GetEdges () const
{
  return Habitat::GetEdges (m_f1, m_f1, m_radius);
}

std::vector<Vector> Habitat::GetNearestPoints (Vector fMax, Vector fMin, double d, Vector target)
{
  return {
      Vector({(fMin.x + fMax.x - fMin.y - fMax.y + 2 * fMax.y - d) / 2, fMax.y, 0}),
      Vector({target.x, (-fMax.x - fMin.x + fMin.y + fMax.y + 2 * target.x + d) / 2, 0}),
      Vector({target.x, (fMin.x - fMax.x + fMin.y + fMax.y + d) / 2, 0}),
      Vector({target.x, (fMin.x + fMax.x + fMin.y + fMax.y - 2 * target.x + d) / 2, 0}),
      Vector({(fMin.x +  fMax.x + fMin.y + fMax.y - 2 * fMax.y + d) / 2, fMax.y, 0}),
      Vector({(fMin.x + fMax.x - fMin.y + fMax.y - d) / 2, target.y, 0}),
      Vector({target.x, target.y, 0}),
      Vector({(fMin.x + fMax.x + fMin.y - fMax.y + d) / 2, target.y, 0}),
      Vector({(fMin.x + fMax.x + fMin.y + fMax.y - 2 * fMin.y - d) / 2, fMin.y, 0}),
      Vector({target.x, (fMin.x + fMax.x + fMin.y + fMax.y - 2 * target.x - d) / 2, 0}),
      Vector({target.x, (-fMin.x + fMax.x + fMin.y + fMax.y - d) / 2, 0}),
      Vector({target.x, (-fMin.x - fMax.x + fMax.y + fMax.y + 2 * target.x - d) / 2, 0}),
      Vector({(fMin.x + fMax.x - fMin.y + 2 * fMin.y + d) / 2, fMin.y, 0})
  };
}

std::pair<Vector, uint8_t> Habitat::GetNearestPoint (Vector fMax, Vector fMin, std::pair<double, double> edges,
                                    double d, Vector target)
{
  if (target.y >= fMax.y){
      if (target.x < edges.second)
        return std::make_pair (Vector({(fMin.x + fMax.x - fMin.y - fMax.y + 2 * fMax.y - d) / 2, fMax.y, 0}), 0);
      else if (target.x >= edges.second && target.x <= fMin.x)
        return std::make_pair (Vector({target.x, (-fMax.x - fMin.x + fMin.y + fMax.y + 2 * target.x + d) / 2, 0}), 1);
      else if (target.x > fMin.x && target.x < fMax.x)
        return std::make_pair (Vector({target.x, (fMin.x - fMax.x + fMin.y + fMax.y + d) / 2, 0}), 2);
      else if (target.x >= fMax.x && target.x <= edges.first)
        return std::make_pair (Vector({target.x, (fMin.x + fMax.x + fMin.y + fMax.y - 2 * target.x + d) / 2, 0}), 3);
      else
        return std::make_pair (Vector({(fMin.x +  fMax.x + fMin.y + fMax.y - 2 * fMax.y + d) / 2, fMax.y, 0}), 4);
    }
  else if (target.y < fMax.y && target.y > fMin.y){
      if (target.x <= fMin.x)
        return std::make_pair (Vector({(fMin.x + fMax.x - fMin.y + fMax.y - d) / 2, target.y, 0}), 5);
      else if (target.x > fMin.x && target.y < fMax.x)
        return std::make_pair (Vector({target.x, target.y, 0}), 6);
      else
        return std::make_pair (Vector({(fMin.x + fMax.x + fMin.y - fMax.y + d) / 2, target.y, 0}), 7);
    }
  else {
      if (target.x < edges.second)
        return std::make_pair (Vector({(fMin.x + fMax.x + fMin.y + fMax.y - 2 * fMin.y - d) / 2, fMin.y, 0}), 8);
      else if (target.x >= edges.second && target.x <= fMin.x)
        return std::make_pair (Vector({target.x, (fMin.x + fMax.x + fMin.y + fMax.y - 2 * target.x - d) / 2, 0}), 9);
      else if (target.x > fMin.x && target.x < fMax.x)
        return std::make_pair (Vector({target.x, (-fMin.x + fMax.x + fMin.y + fMax.y - d) / 2, 0}), 10);
      else if (target.x >= fMax.x && target.x <= edges.first)
        return std::make_pair (Vector({target.x, (-fMin.x - fMax.x + fMax.y + fMax.y + 2 * target.x - d) / 2, 0}), 11);
      else
        return std::make_pair (Vector({(fMin.x + fMax.x - fMin.y + 2 * fMin.y + d) / 2, fMin.y, 0}), 12);
    }
}

std::pair<Vector, uint8_t> Habitat::GetNearestPoint (Vector target) const
{
  auto orderedFoci = GetOrderedFoci();
  auto edges = GetEdges();
  return Habitat::GetNearestPoint (orderedFoci.first, orderedFoci.second, edges, m_radius, target);
}

double Habitat::CalculateDistance (Vector point, Vector target)
{
  if (point.x >= target.x){
      if (point.y >= target.y)
        return  point.x - target.x + point.y - target.y;
      else
        return point.x - target.x - point.y + target.y;
    }
  else{
      if (point.y >= target.y)
        return -point.x + target.x + point.y - target.y;
      else
        return -point.x + target.x - point.y + target.y;
    }
}

double Habitat::GetRadius () const
{
  return m_radius;
}

Vector Habitat::GetF1 ()
{
  return m_f1;
}

Vector Habitat::GetF2 ()
{
  return m_f2;
}

void Habitat::SetF1 (Vector f)
{
  m_f1 = f;
}

void Habitat::SetF2 (Vector f)
{
  m_f2 = f;
}

void Habitat::SetRadius (double radius)
{
  m_radius = radius;
}

void
Habitat::Print (std::ostream &os) const
{
  os << "F1(" << m_f1.x << ", " << m_f1.y
     << ") F2(" << m_f2.x << ", " << m_f2.y
     << ") R(" << m_radius << ")";
}

std::ostream &
operator<< (std::ostream &os, const Habitat &habitat)
{
  habitat.Print (os);
  return os;
}
} // ns3::hrp

