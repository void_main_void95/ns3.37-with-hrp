#ifndef NS3_HRP_HABITAT_H
#define NS3_HRP_HABITAT_H

#include "ns3/vector.h"
#include <vector>

namespace ns3::hrp {
class Habitat {
public:
  Habitat() {};
  Habitat(Vector f1, Vector f2, double radius);
  std::pair<Vector, Vector> GetOrderedFoci () const;
  static std::pair<Vector, Vector> GetOrderedFoci (Vector f1, Vector f2);
  static std::pair<double, double> GetEdges (Vector f1, Vector f2, double radius);
  std::pair<double, double> GetEdges () const;
  static std::pair<Vector, uint8_t> GetNearestPoint (Vector fMax, Vector fMin,
                                                     std::pair<double, double> edges, double d,
                                                     Vector target);
  static std::vector<Vector> GetNearestPoints (Vector fMax, Vector fMin, double d, Vector target);
  std::pair<Vector, uint8_t> GetNearestPoint(Vector target) const;
  static double CalculateDistance (Vector point, Vector target);

  Vector GetF1();
  void SetF1(Vector f);

  Vector GetF2();
  void SetF2(Vector f);

  double GetRadius() const;
  void SetRadius(double radius);
  virtual void Print (std::ostream& os) const;

private:
  Vector m_f1;
  Vector m_f2;
  double m_radius;
};
std::ostream& operator<<(std::ostream& os, const Habitat& habitat);
} // ns3::hrp
#endif //NS3_HRP_HABITAT_H
