#include "hrp-header.h"

#include <utility>

namespace ns3::hrp {
NS_OBJECT_ENSURE_REGISTERED (HrpHeader);

HrpHeader::HrpHeader ()
{
  m_type = MessageType::BEACON;
  m_searchId = 0;
}

HrpHeader::HrpHeader (HrpHeader::MessageType type) : HrpHeader()
{
  SetMessageType (type);
}

HrpHeader::HrpHeader (HrpHeader::MessageType type, PayloadType payload) : HrpHeader (type)
{
  SetPayload (std::move(payload));
}

HrpHeader::HrpHeader (HrpHeader::MessageType type, PayloadType payload, uint32_t searchId) : HrpHeader (type, std::move(payload))
{
  SetSearchId (searchId);
}

TypeId
HrpHeader::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::HrpHeader")
                          .SetParent<Header> ()
                          .SetGroupName ("Internet")
                          .AddConstructor<HrpHeader> ();
  return tid;
}
TypeId
HrpHeader::GetInstanceTypeId () const
{
  return GetTypeId();
}

void
HrpHeader::Print (std::ostream &os) const
{
  os << "| " << m_type;
  os << " | " << m_searchId;
  for (double iter : m_payload)
    {
      os << " | ";
      os << iter;
    }
  os << " |";
}

uint32_t
HrpHeader::GetSerializedSize () const
{
  return sizeof (uint8_t) + sizeof (uint32_t) + m_payload.size() * sizeof (double);
}

void
HrpHeader::Serialize (Buffer::Iterator start) const
{
  Buffer::Iterator i = start;
  i.WriteU8 (static_cast<uint8_t >(m_type));
  i.WriteHtonU32(m_searchId);
  for (double iter : m_payload)
    {
      i.Write(reinterpret_cast<uint8_t *>(&iter), sizeof(double));
    }
}

uint32_t
HrpHeader::Deserialize (Buffer::Iterator start)
{
  Buffer::Iterator i = start;
  auto temp = i.ReadU8 ();
  m_type = MessageType(temp);
  m_searchId = i.ReadNtohU32();
  size_t payloadSize = 0;
  switch(m_type){
    case BEACON:
    case ANNOUNCE:
      return GetSerializedSize ();
    case COMPARE_ME:
      payloadSize = 4;
      break;
    case COMPARE_ME_RESPONSE:
      payloadSize = 7;
      break;
    case SEARCH:
      payloadSize = 16;
      break;
    case SEARCH_RESPONSE:
    case EVALUATE:
      payloadSize = 26;
      break;
    case EVALUATE_RESPONSE:
      payloadSize = 1;
      break;
    default:
      return GetSerializedSize ();
    }
  for (size_t j = 0; j < payloadSize; j++){
      double item;
      i.Read (reinterpret_cast<uint8_t *>(&item), sizeof(double));
      m_payload.push_back(item);
    }
  return GetSerializedSize ();
}

void
HrpHeader::SetMessageType (HrpHeader::MessageType type)
{
  m_type = type;
}

HrpHeader::MessageType
HrpHeader::GetMessageType ()
{
  return m_type;
}

void
HrpHeader::SetPayload (PayloadType payload)
{
  m_payload = std::move(payload);
}

PayloadType
HrpHeader::GetPayload ()
{
  return m_payload;
}

void
HrpHeader::SetSearchId (uint32_t searchId)
{
  m_searchId = searchId;
}

uint32_t
HrpHeader::GetSearchId () const
{
  return m_searchId;
}

bool
HrpHeader::IsValid ()
{
  switch (m_type)
    {
    case BEACON:
    case ANNOUNCE:
      return m_searchId == 0 && m_payload.empty();
    case COMPARE_ME:
      return m_searchId != 0 && m_payload.size() == 4;
    case COMPARE_ME_RESPONSE:
      return m_searchId != 0 && m_payload.size() == 7;
    case SEARCH:
      return m_searchId != 0 && m_payload.size() == 16;
    case SEARCH_RESPONSE:
    case EVALUATE:
      return m_searchId != 0 && m_payload.size() == 26;
    case EVALUATE_RESPONSE:
      return m_searchId != 0 && m_payload.size() == 1;
    default:
      return false;
    }
}

std::ostream &
operator<< (std::ostream & os, HrpHeader const & h)
{
  h.Print (os);
  return os;
} // ns3::hrp
}