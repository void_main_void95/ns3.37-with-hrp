#ifndef HRP_HEADER_H
#define HRP_HEADER_H


#include <vector>
#include "ns3/header.h"
#include "ns3/ipv4-address.h"
#include "ns3/packet.h"
#include "ns3/ipv4-header.h"

namespace ns3::hrp {
  typedef std::vector<double> PayloadType;
  class HrpHeader : public Header {
  public:
    enum MessageType {
      BEACON = 0x1,
      ANNOUNCE = 0x2,
      COMPARE_ME = 0x3,
      COMPARE_ME_RESPONSE = 0x4,
      SEARCH = 0x5,
      SEARCH_RESPONSE = 0x6,
      EVALUATE = 0x7,
      EVALUATE_RESPONSE = 0x8
    };
    HrpHeader();
    explicit HrpHeader(MessageType type);
    explicit HrpHeader(MessageType type, PayloadType payload);
    explicit HrpHeader(MessageType type, PayloadType payloadType, uint32_t searchId);
    static TypeId GetTypeId ();
    TypeId GetInstanceTypeId () const override;
    void Print (std::ostream& os) const override;
    uint32_t GetSerializedSize () const override;
    void Serialize (Buffer::Iterator start) const override;
    uint32_t Deserialize (Buffer::Iterator start) override;
    void SetMessageType(MessageType type);
    MessageType GetMessageType();
    void SetPayload(PayloadType payload);
    PayloadType GetPayload();
    uint32_t GetSearchId() const;
    void SetSearchId(uint32_t searchId);
    bool IsValid();

  private:
    MessageType m_type;
    uint32_t m_searchId;
    PayloadType m_payload;
  };
  std::ostream & operator<< (std::ostream & os, HrpHeader const & h);
}

#endif //HRP_HEADER_H
