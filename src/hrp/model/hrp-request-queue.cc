/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */

#include "hrp-request-queue.h"
#include <algorithm>
#include <functional>
#include "ns3/ipv4-route.h"
#include "ns3/socket.h"
#include "ns3/log.h"

NS_LOG_COMPONENT_DEFINE ("HrpRequestQueue");

namespace ns3::hrp {
uint32_t
HrpRequestQueue::GetSize ()
{
  Purge ();
  return m_queue.size ();
}

bool
HrpRequestQueue::Enqueue (RequestQueueItem & entry)
{
  Purge ();
  for (const auto & item : m_queue)
    {
      if ((item.GetPacket ()->GetUid () == entry.GetPacket ()->GetUid ())
          && (item.GetIpv4Header ().GetDestination ()
              == entry.GetIpv4Header ().GetDestination ()))
        return false;
    }
  entry.SetExpireTime (m_queueTimeout);
  if (m_queue.size () == m_maxLen)
    {
      Drop (m_queue.front (), "Drop the most aged packet");     // Drop the most aged packet
      m_queue.erase (m_queue.begin ());
    }
  m_queue.push_back (entry);
  return true;
}

void
HrpRequestQueue::DropPacketWithDst (Ipv4Address dst)
{
  NS_LOG_FUNCTION (this << dst);
  Purge ();
  for (auto & item : m_queue)
    {
      if (IsEqual (item, dst))
        {
          Drop (item, "DropPacketWithDst ");
        }
    }
  m_queue.erase (std::remove_if (m_queue.begin (), m_queue.end (),
                                 [dst](const RequestQueueItem &en){ return HrpRequestQueue::IsEqual (en, dst); }),
                                 m_queue.end ());
}

bool
HrpRequestQueue::Dequeue (Ipv4Address dst, RequestQueueItem & entry)
{
  Purge ();
  for (auto &item: m_queue){
      if (item.GetIpv4Header ().GetDestination () == dst) {
          entry = item;
          m_queue.erase (std::remove (m_queue.begin(), m_queue.end(), item), m_queue.end());
          return true;
        }
    }
  return false;
}

bool
HrpRequestQueue::Find (Ipv4Address dst)
{
  return std::any_of (m_queue.begin(), m_queue.end(), [dst](const RequestQueueItem &en){ return en.GetIpv4Header ().GetDestination () == dst;});
}

struct IsExpired
{
  bool
  operator() (RequestQueueItem const & e) const
  {
    return (e.GetExpireTime () < Seconds (0));
  }
};

void
HrpRequestQueue::Purge ()
{
  IsExpired pred;
  for (auto & item : m_queue)
    {
      if (pred (item))
        {
          Drop (item, "Drop outdated packet ");
        }
    }
  m_queue.erase (std::remove_if (m_queue.begin (), m_queue.end (), pred),
                 m_queue.end ());
}

void
HrpRequestQueue::Drop (RequestQueueItem en, std::string reason)
{
  NS_LOG_LOGIC (reason << en.GetPacket ()->GetUid () << " " << en.GetIpv4Header ().GetDestination ());
  en.GetErrorCallback () (en.GetPacket (), en.GetIpv4Header (),
                          Socket::ERROR_NOROUTETOHOST);
}

}
