#include "hrp-routing-table.h"
#include "ns3/simulator.h"
#include "ns3/log.h"
#include <algorithm>

NS_LOG_COMPONENT_DEFINE ("HrpRoutingTable");


namespace ns3::hrp {

/*
  Hrp routing table
*/

HrpRoutingTable::HrpRoutingTable ()
{
  m_txErrorCallback = MakeCallback (&HrpRoutingTable::ProcessTxError, this);
  m_entryLifeTime = Seconds (2); //FIXME fazer isto parametrizavel de acordo com tempo de hello
}

/**
 * \brief Adds entry in position table
 */
void HrpRoutingTable::AddEntry (Ipv4Address id, Vector position)
{
  auto i = m_table.find (position);
  if (i != m_table.end () || position == i->first)
    {
      m_table.erase (position);
      m_table.insert (std::make_pair (position, std::make_pair (id, Simulator::Now ())));
      return;
    }
  m_table.insert (std::make_pair (position, std::make_pair (id, Simulator::Now ())));
}

/**
 * \brief remove entries with expired lifetime
 */
void HrpRoutingTable::Purge ()
{

  if(m_table.empty ())
    {
      return;
    }

  std::list<Vector> toErase;

  for (const auto& item : m_table){
      Time delta;
      if (item.second.first == Ipv4Address::GetZero()) delta = m_entryLifeTime + Time (Seconds (0));
      else delta = m_entryLifeTime + item.second.second;
      if (delta <= Simulator::Now ())
        {
          toErase.insert (toErase.begin (), item.first);
        }
    }
  toErase.unique ();
  for (auto item: toErase){
      m_table.erase (item);
    }
}

void HrpRoutingTable::DeleteEntry (Vector position)
{
  m_table.erase (position);
}

/**
 * \brief clears all entries
 */
void HrpRoutingTable::Clear ()
{
  m_table.clear ();
}

/**
 * \ProcessTxError
 */
void HrpRoutingTable::ProcessTxError (WifiMacHeader const & hdr)
{
}

/**
 * \brief Returns true if is in search for destionation
 */
bool HrpRoutingTable::IsInSearch (Ipv4Address id)
{
  return false;
}

bool HrpRoutingTable::HasPosition (Ipv4Address id)
{
  return true;
}

Ipv4Address HrpRoutingTable::Lookup (Vector pos)
{
  auto item = m_table.find (pos);
  if (item != m_table.end()) return item->second.first;
  return Ipv4Address::GetZero();
}

void
HrpRoutingTable::Print (std::ostream &os) const
{
  for (auto &item: m_table){
      os << "| " << item.first.x << ", " << item.first.y
         << " | " << item.second.first << " | " << item.second.second << " |" << std::endl;
    }
}
std::ostream & operator<< (std::ostream & os, HrpRoutingTable const & h)
{
  h.Print (os);
  return os;
}

}   // ns3::hrp
