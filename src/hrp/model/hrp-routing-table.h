#ifndef HRP_ROUTING_TABLE_H
#define HRP_ROUTING_TABLE_H

#include <map>
#include <cassert>
#include <cstdint>
#include "ns3/ipv4.h"
#include "ns3/timer.h"
#include <sys/types.h>
#include "ns3/node.h"
#include "ns3/node-list.h"
#include "ns3/mobility-model.h"
#include "ns3/vector.h"
#include "ns3/wifi-mac-header.h"
#include "ns3/random-variable-stream.h"
#include <complex>

namespace ns3::hrp {

/*
 * \ingroup hrp
 * \brief Position table used by Hrp
 */
class HrpRoutingTable
{
public:
  typedef std::map<Vector, std::pair<Ipv4Address, Time>> TableType;
  /// c-tor
  HrpRoutingTable ();

  /**
   * \brief Adds entry in position table
   */
  void AddEntry (Ipv4Address id, Vector position);

  /**
   * \brief Deletes entry in position table
   */
  void DeleteEntry (Vector position);

  Ipv4Address Lookup(Vector pos);

  /**
   * \brief remove entries with expired lifetime
   */
  void Purge ();

  /**
   * \brief clears all entries
   */
  void Clear ();

  /**
   * \Get Callback to ProcessTxError
   */
  Callback<void, WifiMacHeader const &> GetTxErrorCallback () const
  {
    return m_txErrorCallback;
  }

  bool IsInSearch (Ipv4Address id);

  bool HasPosition (Ipv4Address id);

  static Vector GetInvalidPosition ()
  {
    return {-1, -1, 0};
  }

  void Print (std::ostream& os) const;

private:
  Time m_entryLifeTime;
  TableType m_table;
  // TX error callback
  Callback<void, WifiMacHeader const &> m_txErrorCallback;
  // Process layer 2 TX error notification
  void ProcessTxError (WifiMacHeader const&);
};
std::ostream & operator<< (std::ostream & os, HrpRoutingTable const & h);
} // ns3::hrp
#endif /* HRP_ROUTING_TABLE_H */
