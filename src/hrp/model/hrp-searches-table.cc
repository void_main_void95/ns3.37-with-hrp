#include "hrp-searches-table.h"

namespace ns3::hrp {
SearchesTableItem::~SearchesTableItem ()
{
  m_payloadTable.clear();
  m_vsTable.clear();
}

SearchesTableItem::SearchesTableItem ()
{
  m_pendingCompareMe = 0;
}

PayloadTable &
SearchesTableItem::GetPayloadTable ()
{
  return m_payloadTable;
}

const PayloadTable &
SearchesTableItem::GetPayloadTable () const
{
  return m_payloadTable;
}

size_t
SearchesTableItem::GetPendingCompareMe () const
{
  return m_pendingCompareMe;
}

VsTable &
SearchesTableItem::GetVsTable ()
{
  return m_vsTable;
}

TargetData &
SearchesTableItem::GetTargetData ()
{
  return m_targetData;
}

void
SearchesTableItem::SetPayloadTable (const PayloadTable &payloadTable)
{
  m_payloadTable = payloadTable;
}

void
SearchesTableItem::SetPendingCompareMe (size_t pendingCompareMe)
{
  m_pendingCompareMe = pendingCompareMe;
}

void
SearchesTableItem::SetVsTable (const VsTable &vsTable)
{
  m_vsTable = vsTable;
}

void
SearchesTableItem::SetTargetData (const TargetData &targetData)
{
  m_targetData = targetData;
}

SearchesTableItem::SearchesTableItem (const SearchesTableItem &searchTableItem)
{
  m_payloadTable = searchTableItem.m_payloadTable;
  m_pendingCompareMe = searchTableItem.m_pendingCompareMe;
  m_vsTable = searchTableItem.m_vsTable;
  m_targetData = searchTableItem.m_targetData;
}

SearchesTableItem::SearchesTableItem (SearchesTableItem && searchTableItem)
{
  m_payloadTable = std::move(searchTableItem.m_payloadTable);
  m_pendingCompareMe = searchTableItem.m_pendingCompareMe;
  m_vsTable = boost::move(searchTableItem.m_vsTable);
  m_targetData = std::move(searchTableItem.m_targetData);
}

SearchesTableItem &
SearchesTableItem::operator= (const SearchesTableItem & searchTableItem)
{
  m_payloadTable = searchTableItem.m_payloadTable;
  m_pendingCompareMe = searchTableItem.m_pendingCompareMe;
  m_vsTable = searchTableItem.m_vsTable;
  m_targetData = searchTableItem.m_targetData;
  return *this;
}

const VsTable &
SearchesTableItem::GetVsTable () const
{
  return m_vsTable;
}

const TargetData &
SearchesTableItem::GetTargetData () const
{
  return m_targetData;
}

void
HrpSearchesTable::Add (uint32_t searchId, ns3::Vector position)
{
  m_lookupTable.insert (LookupTable::value_type(searchId, position));
  m_dataTable.insert (std::make_pair (searchId, SearchesTableItem ()));
}

SearchesTableItem &
HrpSearchesTable::Get (uint32_t searchId)
{
  return m_dataTable.at (searchId);
}

SearchesTableItem &
HrpSearchesTable::Get (ns3::Vector position)
{
  auto searchId = m_lookupTable.right.at (position);
  return m_dataTable.at (searchId);
}

void
HrpSearchesTable::Set (uint32_t searchId, const SearchesTableItem &searchTableItem)
{
  m_dataTable.at(searchId) = searchTableItem;
}

void
HrpSearchesTable::Set (ns3::Vector position, const SearchesTableItem &searchTableItem)
{
  auto searchId = m_lookupTable.right.at (position);
  Set (searchId, searchTableItem);
}

void
HrpSearchesTable::Remove (uint32_t searchId)
{
  m_dataTable.erase (searchId);
  m_lookupTable.left.erase (searchId);
}

void
HrpSearchesTable::Remove (ns3::Vector position)
{
  auto searchId = m_lookupTable.right.at (position);
  m_dataTable.erase (searchId);
  m_lookupTable.right.erase (position);
}

HrpSearchesTable::~HrpSearchesTable ()
{
  Clear();
}

void
HrpSearchesTable::Clear ()
{
  m_lookupTable.clear();
  m_dataTable.clear();
}

LookupTable::iterator
HrpSearchesTable::begin ()
{
  return m_lookupTable.begin();
}

LookupTable::const_iterator
HrpSearchesTable::begin () const
{
  return m_lookupTable.begin();
}

LookupTable::iterator
HrpSearchesTable::end ()
{
  return  m_lookupTable.end();
}

LookupTable::const_iterator
HrpSearchesTable::end () const
{
  return m_lookupTable.end();
}

bool
HrpSearchesTable::Exist (uint32_t searchId)
{
  auto item = m_lookupTable.left.find (searchId);
  return item != m_lookupTable.left.end();
}

bool
HrpSearchesTable::Exist (ns3::Vector position)
{
  auto item = m_lookupTable.right.find (position);
  return item != m_lookupTable.right.end();
}

const SearchesTableItem &
HrpSearchesTable::Get (uint32_t searchId) const
{
  auto item = m_dataTable.find (searchId);
  return item->second;;
}

const SearchesTableItem &
HrpSearchesTable::Get (ns3::Vector position) const
{
  auto searchId = m_lookupTable.right.at (position);
  return m_dataTable.at (searchId);
}

HrpSearchesTable::HrpSearchesTable (const HrpSearchesTable &searchesTable)
{
  m_lookupTable = searchesTable.m_lookupTable;
  m_dataTable = searchesTable.m_dataTable;
}

Vector
HrpSearchesTable::GetPosition (uint32_t searchId)
{
  return m_lookupTable.left.at (searchId);
}

const Vector
HrpSearchesTable::GetPosition (uint32_t searchId) const
{
  return m_lookupTable.left.at (searchId);
}
}