#ifndef NS3_SEARCHTABLE_H
#define NS3_SEARCHTABLE_H

#include <boost/bimap.hpp>
#include "ns3/ipv4-address.h"
#include "ns3/vector.h"
#include <map>

namespace ns3::hrp {
typedef std::map<Ipv4Address, std::vector<double>> PayloadTable;
typedef boost::bimap<Ipv4Address, Ipv4Address> VsTable;
typedef std::tuple<Vector, uint32_t, double> TargetData;

class SearchesTableItem
{
public:
  SearchesTableItem ();
  SearchesTableItem (const SearchesTableItem & searchTableItem);
  SearchesTableItem (SearchesTableItem && searchTableItem);
  ~SearchesTableItem ();
  PayloadTable& GetPayloadTable();
  const PayloadTable& GetPayloadTable() const;
  size_t GetPendingCompareMe() const;
  VsTable& GetVsTable();
  const VsTable& GetVsTable() const;
  TargetData &GetTargetData ();
  const TargetData &GetTargetData () const;
  void SetPayloadTable(const PayloadTable &payloadTable);
  void SetPendingCompareMe(size_t pendingCompareMe);
  void SetVsTable(const VsTable& vsTable);
  void SetTargetData(const TargetData &targetData);
  SearchesTableItem & operator=(const SearchesTableItem & searchTableItem);

private:
  PayloadTable m_payloadTable;
  size_t m_pendingCompareMe;
  VsTable m_vsTable;
  TargetData m_targetData;
};

typedef std::map<uint32_t, SearchesTableItem> DataTable;
typedef boost::bimap<uint32_t, Vector> LookupTable;

class HrpSearchesTable
{
public:
  HrpSearchesTable () = default;
  HrpSearchesTable (const HrpSearchesTable & searchesTable);
  HrpSearchesTable (HrpSearchesTable && searchesTable) = delete;
  ~HrpSearchesTable ();
  void Add(uint32_t searchId, Vector position);
  void Remove(uint32_t searchId);
  void Remove(Vector position);
  SearchesTableItem &Get(uint32_t searchId);
  SearchesTableItem &Get(Vector position);
  const SearchesTableItem &Get(uint32_t searchId) const;
  const SearchesTableItem &Get(Vector position) const;
  Vector GetPosition(uint32_t searchId);
  const Vector GetPosition(uint32_t searchId) const;
  void Set(uint32_t searchId, const SearchesTableItem &searchTableItem);
  void Set(Vector position, const SearchesTableItem &searchTableItem);
  void Clear();
  LookupTable::iterator begin();
  LookupTable::const_iterator begin() const;
  LookupTable::iterator end();
  LookupTable::const_iterator end() const;
  bool Exist(uint32_t searchId);
  bool Exist(Vector position);

private:
  LookupTable m_lookupTable;
  DataTable  m_dataTable;
};
}

#endif //NS3_SEARCHTABLE_H
