/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * HRP
 *
 */
#define NS_LOG_APPEND_CONTEXT                                           \
  if (m_ipv4) { std::clog << "[node " << m_ipv4->GetObject<Node> ()->GetId () << "] "; }

#include "hrp.h"
#include "ns3/log.h"
#include "ns3/boolean.h"
#include "ns3/random-variable-stream.h"
#include "ns3/inet-socket-address.h"
#include "ns3/trace-source-accessor.h"
#include "ns3/udp-socket-factory.h"
#include "ns3/wifi-net-device.h"
#include "ns3/adhoc-wifi-mac.h"
#include "ns3/enum.h"
#include <algorithm>
#include <limits>

#define HRP_LS_GOD 0

namespace ns3::hrp {

NS_LOG_COMPONENT_DEFINE ("HrpRoutingProtocol");

struct DeferredRouteOutputTag : public Tag
{
  /// Positive if output device is fixed in RouteOutput
  uint32_t m_isCallFromL3;

  DeferredRouteOutputTag () : Tag (), m_isCallFromL3 (0)
  {
  }

  static TypeId
  GetTypeId ()
  {
    static TypeId tid = TypeId ("ns3::hrp::DeferredRouteOutputTag").SetParent<Tag> ();
    return tid;
  }

  TypeId
  GetInstanceTypeId () const override
  {
    return GetTypeId ();
  }

  uint32_t
  GetSerializedSize () const override
  {
    return sizeof (uint32_t);
  }

  void
  Serialize (TagBuffer i) const override
  {
    i.WriteU32 (m_isCallFromL3);
  }

  void
  Deserialize (TagBuffer i) override
  {
    m_isCallFromL3 = i.ReadU32 ();
  }

  void
  Print (std::ostream &os) const override
  {
    os << "DeferredRouteOutputTag: m_isCallFromL3 = " << m_isCallFromL3;
  }
};

NS_OBJECT_ENSURE_REGISTERED (HrpRoutingProtocol);

/// UDP Port for HRP control traffic, not defined by IANA yet
const uint32_t HrpRoutingProtocol::HRP_PORT = 666;

HrpRoutingProtocol::HrpRoutingProtocol ()
    : AskForPresenceInterval (Seconds (1)),
      MaxQueueLen (64),
      MaxQueueTime (Seconds (30)),
      m_queue (MaxQueueLen, MaxQueueTime),
      LocationServiceName (HRP_LS_GOD)
{
  m_rng = CreateObject<UniformRandomVariable> ();
}

TypeId
HrpRoutingProtocol::GetTypeId ()
{
  static TypeId tid =
      TypeId ("ns3::hrp::RoutingProtocol")
          .SetParent<Ipv4RoutingProtocol> ()
          .AddConstructor<HrpRoutingProtocol> ()
          .AddAttribute ("AskForPresenceInterval", "Ask for presence messages emission interval.",
                         TimeValue (Seconds (1)),
                         MakeTimeAccessor (&HrpRoutingProtocol::AskForPresenceInterval),
                         MakeTimeChecker ())
          .AddAttribute ("LocationServiceName", "Indicates which Location Service is enabled",
                         EnumValue (HRP_LS_GOD),
                         MakeEnumAccessor (&HrpRoutingProtocol::LocationServiceName),
                         MakeEnumChecker (HRP_LS_GOD, "GOD"));
  return tid;
}

HrpRoutingProtocol::~HrpRoutingProtocol () = default;

void
HrpRoutingProtocol::DoDispose ()
{
  m_socketAddresses.clear ();
  m_discoveredNeighbors.clear ();
  m_regionsTable.clear ();
  m_routingTable.Clear ();
  m_searchesTable.Clear ();
  m_queuedAddresses.clear ();
  AskForPresenceTimer.Cancel ();
  CheckQueueTimer.Cancel ();
  m_ipv4 = nullptr;
  Ipv4RoutingProtocol::DoDispose ();
}

Ptr<LocationService>
HrpRoutingProtocol::GetLS ()
{
  return m_locationService;
}

void
HrpRoutingProtocol::SetLS (Ptr<LocationService> locationService)
{
  m_locationService = locationService;
}

bool
HrpRoutingProtocol::RouteInput (Ptr<const Packet> p, const Ipv4Header &header,
                             Ptr<const NetDevice> idev, UnicastForwardCallback ucb,
                             MulticastForwardCallback mcb, LocalDeliverCallback lcb,
                             ErrorCallback ecb)
{

  NS_LOG_FUNCTION (this << p->GetUid () << header.GetDestination () << idev->GetAddress ());
  if (m_socketAddresses.empty ())
    {
      NS_LOG_LOGIC ("No hrp interfaces");
      return false;
    }
  NS_ASSERT (m_ipv4 != nullptr);
  NS_ASSERT (p != nullptr);
  // Check if input device supports IP
  NS_ASSERT (m_ipv4->GetInterfaceForDevice (idev) >= 0);
  int32_t iif = m_ipv4->GetInterfaceForDevice (idev);
  Ipv4Address dst = header.GetDestination ();
  Ipv4Address origin = header.GetSource ();

  DeferredRouteOutputTag tag;
  if (p->PeekPacketTag (tag) && IsMyOwnAddress (origin))
    {
      Ptr<Packet> packet = p->Copy ();
      packet->RemovePacketTag (tag);
      DeferredRouteOutput (packet, header, ucb, ecb);
      return true;
    }

  if (m_ipv4->IsDestinationAddress (dst, iif))
    {
      if (dst != m_ipv4->GetAddress (1, 0).GetBroadcast ())
        {
          NS_LOG_LOGIC ("Unicast local delivery to " << dst);
        }
      else
        {
          NS_LOG_LOGIC ("Broadcast local delivery to " << dst);
        }
      lcb (p, header, iif);
      return true;
    }
  return Forwarding (p, header, ucb, ecb);
}

void
HrpRoutingProtocol::DeferredRouteOutput (Ptr<const Packet> p, const Ipv4Header &header,
                                      UnicastForwardCallback ucb, ErrorCallback ecb)
{
  NS_LOG_FUNCTION (this << p << header);
  NS_ASSERT (p != nullptr && p != Ptr<Packet> ());

  if (m_queue.GetSize () == 0)
    {
      CheckQueueTimer.Cancel ();
      CheckQueueTimer.Schedule (Time ("500ms"));
    }

  RequestQueueItem newEntry (p, header, ucb, ecb);
  bool result = m_queue.Enqueue (newEntry);

  m_queuedAddresses.insert (m_queuedAddresses.begin (), header.GetDestination ());
  m_queuedAddresses.unique ();

  if (result)
    NS_LOG_LOGIC ("Add packet " << p->GetUid () << " to queue. Protocol "
                                << (uint16_t) header.GetProtocol ());
}

void
HrpRoutingProtocol::CheckQueue ()
{

  CheckQueueTimer.Cancel ();

  std::list<Ipv4Address> toRemove;

  for (auto &item : m_queuedAddresses)
    {
      if (SendPacketFromQueue (item))
        {
          //Insert in a list to remove later
          toRemove.insert (toRemove.begin (), item);
        }
    }

  //remove all that are on the list
  for (auto &item : toRemove)
    {
      m_queuedAddresses.remove (item);
    }

  if (!m_queuedAddresses.empty ()) { //Only need to schedule if the queue is not empty
    CheckQueueTimer.Schedule (Time ("500ms"));
  }
}

bool
HrpRoutingProtocol::SendPacketFromQueue (Ipv4Address dst)
{
  NS_LOG_FUNCTION (this);
  RequestQueueItem queueEntry;

  if (IsInSearch (dst)) {
    return false;
  }

  if (!m_locationService->HasPosition (dst)) // Location-service stoped looking for the dst
    {
      m_queue.DropPacketWithDst (dst);
      NS_LOG_LOGIC ("Location Service did not find dst. Drop packet to " << dst);
      return true;
    }

  Ipv4Address nextHop;

  if (m_discoveredNeighbors.find (dst) != m_discoveredNeighbors.end ())
    {
      nextHop = dst;
    }
  else
    {
      auto dstPos = m_locationService->GetPosition (dst);
      nextHop = m_routingTable.Lookup (dstPos);
    }

  auto route = Create<Ipv4Route> ();
  route->SetDestination (dst);
  route->SetGateway (nextHop);

  // FIXME: Does not work for multiple interfaces
  route->SetOutputDevice (m_ipv4->GetNetDevice (1));

  while (m_queue.Dequeue (dst, queueEntry))
    {
      DeferredRouteOutputTag tag;
      auto p = ConstCast<Packet> (queueEntry.GetPacket ());

      auto ucb = queueEntry.GetUnicastForwardCallback ();
      auto header = queueEntry.GetIpv4Header ();

      if (header.GetSource () == Ipv4Address ("102.102.102.102"))
        {
          route->SetSource (m_ipv4->GetAddress (1, 0).GetLocal ());
        }
      else
        {
          route->SetSource (header.GetSource ());
        }
      ucb (route, p, header);
    }
  return true;
}

void
HrpRoutingProtocol::NotifyInterfaceUp (uint32_t interface)
{
  NS_LOG_FUNCTION (this << m_ipv4->GetAddress (interface, 0).GetLocal ());
  Ptr<Ipv4L3Protocol> l3 = m_ipv4->GetObject<Ipv4L3Protocol> ();
  if (l3->GetNAddresses (interface) > 1)
    {
      NS_LOG_WARN ("HRP does not work with more then one address per each interface.");
    }
  Ipv4InterfaceAddress iface = l3->GetAddress (interface, 0);
  if (iface.GetLocal () == Ipv4Address ("127.0.0.1")) {
    return;
}

  // Create a socket to listen only on this interface
  auto socket = Socket::CreateSocket (GetObject<Node> (), UdpSocketFactory::GetTypeId ());
  NS_ASSERT (socket != nullptr);
  socket->SetRecvCallback (MakeCallback (&HrpRoutingProtocol::RecvHRP, this));
  socket->SetAllowBroadcast (true);
  socket->SetAttribute ("IpTtl", UintegerValue (1));
  auto address = Ipv4Address::GetAny ();
  socket->Bind (InetSocketAddress (address, HRP_PORT));
  socket->BindToNetDevice (l3->GetNetDevice (interface));
  m_socketAddresses.insert (std::make_pair (socket, iface));

  // Allow neighbor manager use this interface for layer 2 feedback if possible
  auto dev = m_ipv4->GetNetDevice (m_ipv4->GetInterfaceForAddress (iface.GetLocal ()));
  auto wifi = dev->GetObject<WifiNetDevice> ();
  if (wifi == nullptr) {
    return;
  }

  auto mac = wifi->GetMac ();
  if (mac == nullptr) {
    return;
  }

  // mac->TraceConnectWithoutContext ("TxErrHeader", m_routingTable.GetTxErrorCallback ());
}

void
HrpRoutingProtocol::RecvHRP (Ptr<Socket> socket)
{
  Address sourceAddress;
  auto packet = socket->RecvFrom (sourceAddress);
  auto inetSourceAddr = InetSocketAddress::ConvertFrom (sourceAddress);
  auto sender = inetSourceAddr.GetIpv4 ();

  HrpHeader header;
  packet->RemoveHeader (header);
  NS_LOG_FUNCTION (this << socket << sender << header);
  if (!header.IsValid ())
    {
      NS_LOG_DEBUG ("Hrp message " << packet->GetUid () << " invalid. Ignored");
      return;
    }

  switch (header.GetMessageType ())
    {
    case HrpHeader::BEACON:
      AskForPresenceTimer.Cancel ();
      UpdateDiscoveredNeighbours (header, socket, sender);
      AskForPresenceTimer.Schedule (
          Seconds (m_rng->GetValue (0.01, AskForPresenceInterval.GetSeconds ())));
      break;
    case HrpHeader::ANNOUNCE:
      UpdateDiscoveredNeighbours (header, socket, sender);
      SendBeaconFrame (sender);
      break;
    case HrpHeader::COMPARE_ME:
      SendCompareMeResponse (header, sender);
      break;
    case HrpHeader::COMPARE_ME_RESPONSE:
      UpdateBetterNeighbors (header, sender);
      break;
    case HrpHeader::SEARCH:
      SendSearchResponse (header, sender);
      break;
    case HrpHeader::SEARCH_RESPONSE:
      SendEvaluate (header, sender);
      break;
    case HrpHeader::EVALUATE:
      SendEvaluateResponse (header, sender);
      break;
    case HrpHeader::EVALUATE_RESPONSE:
      UpdateRoutingTable (header, sender);
      break;
    default:
      NS_LOG_LOGIC ("Ignoring message with unknown type: " << int (header.GetMessageType ()));
      break;
    }
}

void
HrpRoutingProtocol::NotifyInterfaceDown (uint32_t interface)
{
  NS_LOG_FUNCTION (this << m_ipv4->GetAddress (interface, 0).GetLocal ());

  // Disable layer 2 link state monitoring (if possible)
  auto l3 = m_ipv4->GetObject<Ipv4L3Protocol> ();
  auto dev = l3->GetNetDevice (interface);
  auto wifi = dev->GetObject<WifiNetDevice> ();
  if (wifi != nullptr)
    {
      auto mac = wifi->GetMac ()->GetObject<AdhocWifiMac> ();
      if (mac != nullptr) {
        mac->TraceDisconnectWithoutContext ("TxErrHeader", m_routingTable.GetTxErrorCallback ());
      }
    }

  // Close socket
  auto socket = FindSocketWithInterfaceAddress (m_ipv4->GetAddress (interface, 0));
  NS_ASSERT (socket);
  socket->Close ();
  m_socketAddresses.erase (socket);
  if (m_socketAddresses.empty ())
    {
      NS_LOG_LOGIC ("No hrp interfaces");
      m_discoveredNeighbors.clear ();
      m_regionsTable.clear ();
      m_routingTable.Clear ();
      m_locationService->Clear ();
      return;
    }
}

Ptr<Socket>
HrpRoutingProtocol::FindSocketWithInterfaceAddress (Ipv4InterfaceAddress addr) const
{
  NS_LOG_FUNCTION (this << addr);
  for (const auto &item : m_socketAddresses)
    {
      auto socket = item.first;
      Ipv4InterfaceAddress iface = item.second;
      if (iface == addr)
        {
          return socket;
        }
    }
  Ptr<Socket> socket;
  return socket;
}

void
HrpRoutingProtocol::NotifyAddAddress (uint32_t interface, Ipv4InterfaceAddress address)
{
  NS_LOG_FUNCTION (this << interface << address);
  auto l3 = m_ipv4->GetObject<Ipv4L3Protocol> ();
  if (!l3->IsUp (interface)) {
    return ;
  }
  if (l3->GetNAddresses ((interface) == 1))
    {
      auto iface = l3->GetAddress (interface, 0);
      auto socket = FindSocketWithInterfaceAddress (iface);
      if (!socket)
        {
          if (iface.GetLocal () == Ipv4Address ("127.0.0.1")) {
            return ;
          }
          // Create a socket to listen only on this interface
          socket = Socket::CreateSocket (GetObject<Node> (), UdpSocketFactory::GetTypeId ());
          NS_ASSERT (socket != nullptr);
          socket->SetRecvCallback (MakeCallback (&HrpRoutingProtocol::RecvHRP, this));
          socket->SetAllowBroadcast (true);
          socket->SetAttribute ("IpTtl", UintegerValue (1));
          socket->Bind (InetSocketAddress (Ipv4Address::GetAny (), HRP_PORT));
          socket->BindToNetDevice (l3->GetNetDevice (interface));
          m_socketAddresses.insert (std::make_pair (socket, iface));

          auto dev = m_ipv4->GetNetDevice (m_ipv4->GetInterfaceForAddress (iface.GetLocal ()));
        }
    }
  else
    NS_LOG_LOGIC ("HRP does not work with more then one address per each interface. "
                  "Ignore added address");
}

void
HrpRoutingProtocol::NotifyRemoveAddress (uint32_t i, Ipv4InterfaceAddress address)
{
  NS_LOG_FUNCTION (this);
  auto socket = FindSocketWithInterfaceAddress (address);
  if (socket)
    {

      m_socketAddresses.erase (socket);
      auto l3 = m_ipv4->GetObject<Ipv4L3Protocol> ();
      if (l3->GetNAddresses (i))
        {
          auto iface = l3->GetAddress (i, 0);
          // Create a socket to listen only on this interface
          socket = Socket::CreateSocket (GetObject<Node> (), UdpSocketFactory::GetTypeId ());
          NS_ASSERT (socket != nullptr);
          socket->SetRecvCallback (MakeCallback (&HrpRoutingProtocol::RecvHRP, this));
          socket->SetAllowBroadcast (true);
          socket->SetAttribute ("IpTtl", UintegerValue (1));
          socket->Bind (InetSocketAddress (Ipv4Address::GetAny (), HRP_PORT));
          m_socketAddresses.insert (std::make_pair (socket, iface));

          // Add local broadcast record to the routing table
          auto dev = m_ipv4->GetNetDevice (m_ipv4->GetInterfaceForAddress (iface.GetLocal ()));
        }
      if (m_socketAddresses.empty ())
        {
          NS_LOG_LOGIC ("No hrp interfaces");
          m_discoveredNeighbors.clear ();
          m_regionsTable.clear ();
          m_routingTable.Clear ();
          m_locationService->Clear ();
          return;
        }
    }
  else
    NS_LOG_LOGIC ("Remove address not participating in HRP operation");
}

void
HrpRoutingProtocol::SetIpv4 (Ptr<Ipv4> ipv4)
{
  NS_ASSERT (ipv4 != nullptr);
  NS_ASSERT (m_ipv4 == nullptr);

  m_ipv4 = ipv4;
  //Schedule only when it has packets on queue
  Simulator::ScheduleNow (&HrpRoutingProtocol::Start, this);
}

bool
HrpRoutingProtocol::IsMyOwnAddress (Ipv4Address src)
{
  NS_LOG_FUNCTION (this << src);
  for (auto &item : m_socketAddresses)
    {
      Ipv4InterfaceAddress iface = item.second;
      if (src == iface.GetLocal ()) {
        return true;
      }
    }
  return false;
}

void
HrpRoutingProtocol::Start ()
{
  NS_LOG_FUNCTION (this);
  m_queuedAddresses.clear ();

  switch (LocationServiceName)
    {
    case HRP_LS_GOD:
      m_locationService = CreateObject<GodLocationService> ();
      break;
    default:
      NS_LOG_UNCOND ("No location service found.");
      break;
    }

  CheckQueueTimer.SetFunction (&HrpRoutingProtocol::CheckQueue, this);
  AskForPresenceTimer.SetFunction (&HrpRoutingProtocol::SendAskForPresence, this);
  UpdateHabitat ();
  auto delay = Seconds (m_rng->GetValue (0.01, AskForPresenceInterval.GetSeconds ()));
  AskForPresenceTimer.Schedule (delay);
}

Ptr<Ipv4Route>
HrpRoutingProtocol::LoopbackRoute (const Ipv4Header &hdr, Ptr<NetDevice> oif)
{
  NS_LOG_FUNCTION (this << hdr);
  m_lo = m_ipv4->GetNetDevice (0);
  NS_ASSERT (m_lo != nullptr);
  auto rt = Create<Ipv4Route> ();
  rt->SetDestination (hdr.GetDestination ());

  auto j = m_socketAddresses.begin ();
  if (oif)
    {
      // Iterate to find an address on the oif device
      for (j = m_socketAddresses.begin (); j != m_socketAddresses.end (); ++j)
        {
          Ipv4Address addr = j->second.GetLocal ();
          int32_t interface = m_ipv4->GetInterfaceForAddress (addr);
          if (oif == m_ipv4->GetNetDevice (static_cast<uint32_t> (interface)))
            {
              rt->SetSource (addr);
              break;
            }
        }
    }
  else
    {
      rt->SetSource (j->second.GetLocal ());
    }
  NS_ASSERT_MSG (rt->GetSource () != Ipv4Address (), "Valid Hrp source address not found");
  rt->SetGateway (Ipv4Address ("127.0.0.1"));
  rt->SetOutputDevice (m_lo);
  return rt;
}

int
HrpRoutingProtocol::GetProtocolNumber () const
{
  return HRP_PORT;
}

bool
HrpRoutingProtocol::Forwarding (Ptr<const Packet> packet, const Ipv4Header &header,
                             UnicastForwardCallback ucb, ErrorCallback ecb)
{
  NS_LOG_FUNCTION (this << packet << header);
  auto p = packet->Copy ();
  auto dst = header.GetDestination ();
  auto origin = header.GetSource ();
  auto oif = m_ipv4->GetObject<NetDevice> ();

  m_routingTable.Purge ();

  auto dstPos = m_locationService->GetPosition (dst);
  if (CalculateDistance (dstPos, m_locationService->GetInvalidPosition ()) == 0 &&
      m_locationService->IsInSearch (dst))
    {
      DeferredRouteOutputTag tag;
      if (!p->PeekPacketTag (tag))
        {
          p->AddPacketTag (tag);
        }
      DeferredRouteOutput (p, header, ucb, ecb);
      return false;
    }

  Ipv4Address nextHop;
  if (m_discoveredNeighbors.find (dst) != m_discoveredNeighbors.end ())
    {
      nextHop = dst;
    }
  else
    {
      nextHop = m_routingTable.Lookup (dstPos);
    }
  if (nextHop != Ipv4Address::GetZero ())
    {
      auto route = Create<Ipv4Route> ();
      route->SetDestination (dst);
      route->SetSource (header.GetSource ());
      route->SetGateway (nextHop);
      // FIXME: Does not work for multiple interfaces
      route->SetOutputDevice (m_ipv4->GetNetDevice (1));
      route->SetDestination (header.GetDestination ());
      NS_ASSERT (route != nullptr);
      NS_LOG_LOGIC (route->GetOutputDevice ()
                    << " forwarding to " << dst << " from " << origin << " through "
                    << route->GetGateway () << " packet " << p->GetUid ());
      ucb (route, p, header);
      return true;
    }
  else
    {
      SendCompareMe (dstPos);
      DeferredRouteOutputTag tag;
      if (!p->PeekPacketTag (tag))
        {
          p->AddPacketTag (tag);
        }
      DeferredRouteOutput (p, header, ucb, ecb);
      return false;
    }
  return true;
}

void
HrpRoutingProtocol::SetDownTarget (IpL4Protocol::DownTargetCallback callback)
{
  m_downTarget = callback;
}

IpL4Protocol::DownTargetCallback
HrpRoutingProtocol::GetDownTarget () const
{
  return m_downTarget;
}

Ptr<Ipv4Route>
HrpRoutingProtocol::RouteOutput (Ptr<Packet> p, const Ipv4Header &header, Ptr<NetDevice> oif,
                              Socket::SocketErrno &sockerr)
{
  NS_LOG_FUNCTION (this << header << (oif ? oif->GetIfIndex () : 0));

  if (!p)
    {
      return LoopbackRoute (header, oif); // later
    }
  if (m_socketAddresses.empty ())
    {
      sockerr = Socket::ERROR_NOROUTETOHOST;
      NS_LOG_LOGIC ("No hrp interfaces");
      Ptr<Ipv4Route> route;
      return route;
    }
  sockerr = Socket::ERROR_NOTERROR;
  auto route = Create<Ipv4Route> ();
  auto dst = header.GetDestination ();
  auto origin = header.GetSource ();
  auto dstPos = Vector (1, 0, 0);
  Ipv4Address nextHop;

  if (m_discoveredNeighbors.find (dst) != m_discoveredNeighbors.end () || dst == m_ipv4->GetAddress (1, 0).GetBroadcast ())
    {
      nextHop = dst;
    }
  else
    {
      if (IsInSearch (dst))
        {
          DeferredRouteOutputTag tag;
          if (!p->PeekPacketTag (tag))
            {
              p->AddPacketTag (tag);
            }
          return LoopbackRoute (header, oif);
        }
      dstPos = m_locationService->GetPosition (dst);
      nextHop = m_routingTable.Lookup (dstPos);
    }

  if (nextHop != Ipv4Address::GetZero ())
    {
      route->SetDestination (dst);
      if (header.GetSource () == Ipv4Address ("102.102.102.102"))
        {
          route->SetSource (m_ipv4->GetAddress (1, 0).GetLocal ());
        }
      else
        {
          route->SetSource (header.GetSource ());
        }
      route->SetGateway (nextHop);
      route->SetOutputDevice (
          m_ipv4->GetNetDevice (m_ipv4->GetInterfaceForAddress (route->GetSource ())));
      route->SetDestination (header.GetDestination ());
      NS_ASSERT (route != nullptr);
      NS_LOG_LOGIC (route->GetSource()
                    << " forwarding to " << dst << " from " << origin << " through "
                    << route->GetGateway () << " packet " << p->GetUid ());
      if (oif != nullptr && route->GetOutputDevice () != oif)
        {
          NS_LOG_DEBUG ("Output device doesn't match. Dropped.");
          sockerr = Socket::ERROR_NOROUTETOHOST;
          return {};
        }
      return route;
    }
  else
    {
      SendCompareMe (dstPos);
      DeferredRouteOutputTag tag;
      if (!p->PeekPacketTag (tag))
        {
          p->AddPacketTag (tag);
        }
      return LoopbackRoute (header, oif);
    }
}

void
HrpRoutingProtocol::SendUnicast (HrpHeader header, Ipv4Address destination)
{
  NS_LOG_FUNCTION (this << header << destination);
  auto socket = m_discoveredNeighbors.at(destination);
  if (socket)
    {
      auto packet = Create<Packet> ();
      packet->AddHeader (header);
      socket->SendTo (packet, 0, InetSocketAddress (destination, HRP_PORT));
    }
}

void
HrpRoutingProtocol::SendBroadcast (HrpHeader header)
{
  NS_LOG_FUNCTION (this << header);
  for (auto &item : m_socketAddresses)
    {
      auto socket = item.first;
      auto iface = item.second;

      auto packet = Create<Packet> ();
      packet->AddHeader (header);

      // Send to all-hosts broadcast if on /32 addr, subnet-directed otherwise
      Ipv4Address destination;
      if (iface.GetMask () == Ipv4Mask::GetOnes ())
        {
          destination = Ipv4Address ("255.255.255.255");
        }
      else
        {
          destination = iface.GetBroadcast ();
        }
      NS_LOG_FUNCTION (this << header << destination);
      socket->SendTo (packet, 0, InetSocketAddress (destination, HRP_PORT));
    }
}

void
HrpRoutingProtocol::SendCompareMe (Vector target)
{
  NS_LOG_FUNCTION (this << target);
  auto id = this->m_rng->GetInteger (1, std::numeric_limits<uint32_t>::max ());
  auto nearestPoint = m_habitat.GetNearestPoint (target);
  auto currentDistance = Habitat::CalculateDistance (nearestPoint.first, target);
  HrpHeader header (HrpHeader::COMPARE_ME,
                    {target.x, target.y, currentDistance, m_habitat.GetRadius ()}, id);
  PayloadTable record;
  VsTable vsTable;
  for (auto &neighbor : m_discoveredNeighbors){
      record.insert (std::make_pair (neighbor.first, std::vector<double> ()));
    }
  m_searchesTable.Add (id, target);
  SearchesTableItem newItem;
  newItem.SetPayloadTable (record);
  newItem.SetPendingCompareMe (m_discoveredNeighbors.size());
  newItem.SetTargetData (std::make_tuple (nearestPoint.first, nearestPoint.second, currentDistance));
  m_searchesTable.Set (id, newItem);
  for (auto &neighbor : m_discoveredNeighbors){
      SendUnicast (header, neighbor.first);
    }
}

void
HrpRoutingProtocol::SendAskForPresence ()
{
  NS_LOG_FUNCTION (this);
  if (AskForPresenceTimer.IsRunning ())
    {
      NS_LOG_LOGIC ("Skipping ask for presence due to cooldown");
      return;
    }

  HrpHeader header;
  header.SetMessageType (HrpHeader::ANNOUNCE);
  SendBroadcast (header);

  Time delay = Seconds (m_rng->GetValue (0.01, AskForPresenceInterval.GetSeconds ()));
  AskForPresenceTimer.Schedule (delay);
}

void
HrpRoutingProtocol::SendBeaconFrame (Ipv4Address destination)
{
  NS_LOG_FUNCTION (this << destination);
  HrpHeader header;
  header.SetMessageType (HrpHeader::BEACON);
  SendUnicast (header, destination);
}

void
HrpRoutingProtocol::UpdateDiscoveredNeighbours (HrpHeader header, Ptr<Socket> socket,
                                             Ipv4Address address)
{
  NS_LOG_FUNCTION (this << header << socket << address);
  std::vector<double> requestPayload = header.GetPayload ();
  auto existing = m_discoveredNeighbors.find (address);
  if (existing == m_discoveredNeighbors.end ())
    {
      NS_LOG_INFO ("Found new neighbor " << address << " at time " << Simulator::Now().As(Time::S));
      m_discoveredNeighbors.insert (std::make_pair (address, socket));
      for (auto &search: m_searchesTable){
          auto id = search.left;
          auto target = search.right;
          auto item = m_searchesTable.Get (id);
          auto currentDistance = std::get<2>(item.GetTargetData ());
          HrpHeader compareMe (HrpHeader::COMPARE_ME,
                            {target.x, target.y, currentDistance, m_habitat.GetRadius ()}, id);
          item.GetPayloadTable().insert (std::make_pair (address, std::vector<double>()));
          item.SetPendingCompareMe (item.GetPendingCompareMe() + 1);
          m_searchesTable.Set (id, item);
          SendUnicast (compareMe, address);
        }
    }
}

void
HrpRoutingProtocol::SendCompareMeResponse (HrpHeader header, Ipv4Address destination)
{
  NS_LOG_FUNCTION (this << header << destination);
  std::vector<double> requestPayload = header.GetPayload ();
  auto orderedFoci = m_habitat.GetOrderedFoci ();
  auto edges = m_habitat.GetEdges ();
  auto radius = m_habitat.GetRadius ();
  Vector target ({requestPayload.at(0), requestPayload.at(1), 0});
  auto nearestPoint = Habitat::GetNearestPoint (orderedFoci.first, orderedFoci.second,
                                                edges, radius, target);

  auto distance = Habitat::CalculateDistance (nearestPoint.first, target);
  m_regionsTable.insert (std::make_pair (header.GetSearchId (), nearestPoint.second));
  auto f1 = m_habitat.GetF1 ();
  auto f2 = m_habitat.GetF2 ();
  auto otherNodeDistance = requestPayload.at(2);
  auto otherNodeRadius = requestPayload.at(3);
  auto diffDistance = otherNodeDistance - distance;
  auto diffRadius = otherNodeRadius + otherNodeDistance * radius - radius;
  std::vector<double> responsePayload ({diffDistance, diffRadius, f1.x, f1.y, f2.x, f2.y, radius});
  auto flip = this->m_rng->GetInteger (0, 1);
  if (flip)
    {
      responsePayload.at(0) = diffRadius;
      requestPayload.at(1) = diffDistance;
    }
  HrpHeader responseHeader (HrpHeader::COMPARE_ME_RESPONSE, responsePayload, header.GetSearchId ());
  SendUnicast (responseHeader, destination);
}

void
HrpRoutingProtocol::UpdateBetterNeighbors (HrpHeader header, Ipv4Address address)
{
  NS_LOG_FUNCTION (this << header << address);
  auto requestPayload = header.GetPayload ();
  auto requestId = header.GetSearchId ();
  auto searchItem = m_searchesTable.Get (requestId);
  auto record = searchItem.GetPayloadTable();
  auto target = m_searchesTable.GetPosition (requestId);
  auto item = record.find (address);
  if (item == record.end ()){
      return;
  }
  if (requestPayload.at(0) >= 0 && requestPayload.at(1) >= 0)
    {
      Vector f1 ({requestPayload.at(2), requestPayload.at(3), 0});
      Vector f2 ({requestPayload.at(4), requestPayload.at(5), 0});
      auto orderedFoci = Habitat::GetOrderedFoci (f1, f2);
      auto points = Habitat::GetNearestPoints (orderedFoci.first, orderedFoci.second,
                                               requestPayload.at(6), target);
      item->second.clear ();
      item->second.shrink_to_fit();
      for (auto &point : points)
        {
          auto distance = Habitat::CalculateDistance (point, target);
          item->second.push_back (distance);
        }
        item->second.push_back (requestPayload.at(6));
    }
  else
    {
      record.erase (item);
    }
  searchItem.SetPendingCompareMe (searchItem.GetPendingCompareMe() - 1);
  searchItem.SetPayloadTable (record);
  m_searchesTable.Set (requestId, searchItem);
  if (searchItem.GetPendingCompareMe() > 0) {
      return;
    }
  if (record.empty ())
    {
      NS_LOG_INFO ("No nearest node found to target ("
                   << target.x << "," << target.y << ") at time "
                   << Simulator::Now().As(Time::S));
    }
  else if (record.size () == 1)
    {
      m_routingTable.AddEntry (address, target);
      NS_LOG_INFO("Found a nearest node (" << address << ") to target ("
                                            << target.x << ", " << target.y
                                            << ") at time " << Simulator::Now().As(Time::S));
    }
  else
    {
      SendSearch (requestId);
    }
}

void
HrpRoutingProtocol::SendSearch (uint32_t id)
{
  NS_LOG_FUNCTION (this << id);
  auto item = m_searchesTable.Get (id);
  auto record = item.GetPayloadTable ();
  auto target = m_searchesTable.GetPosition (id);
  auto vsTable = item.GetVsTable();

  NS_LOG_INFO ("Searching for a nearest node to target ("
               << target.x << ", " << target.y << ") at time "
               << Simulator::Now().As(Time::S));

  long targetSize = ceil (static_cast<double> (record.size ()) / 2);
  for (auto i = record.begin (); std::distance (record.begin (), i) < targetSize;
       std::advance (i, 2))
    {

      auto phase1Payload = i->second;
      auto j = std::next (i, 1);
      std::vector<double> responsePayload;
      responsePayload.resize (16);
      responsePayload.insert (responsePayload.begin (), target.x);
      responsePayload.insert (responsePayload.begin () + 1, target.y);
      responsePayload.insert (responsePayload.begin () + 2, phase1Payload.begin (),
                              phase1Payload.begin () + 13);
      responsePayload.insert (responsePayload.end (), phase1Payload.at(13));
      vsTable.insert (VsTable::value_type (i->first, j->first));
      item.SetVsTable (vsTable);
      m_searchesTable.Set (id, item);
      HrpHeader header (HrpHeader::SEARCH, responsePayload, id);
      SendUnicast (header, j->first);
    }
}

void
HrpRoutingProtocol::SendSearchResponse (HrpHeader header, Ipv4Address destination)
{
  NS_LOG_FUNCTION (this << header << destination);
  auto requestPayload = header.GetPayload ();
  auto nearestPoint = m_habitat.GetNearestPoint ({requestPayload.at(0), requestPayload.at(1), 0});
  auto distance = Habitat::CalculateDistance (nearestPoint.first,
                                              {requestPayload.at(0), requestPayload.at(1), 0});
  auto radius = m_habitat.GetRadius();
  std::vector<double> responsePayload;
  responsePayload.resize(26);
  for (auto i = 0; i < 13; i++)
    {
      auto otherNodeDistance = requestPayload.at(i + 2);
      auto otherNodeRadius = requestPayload.at(15);
      responsePayload.at(i * 2) = distance - otherNodeDistance;
      responsePayload.at(i * 2 + 1) = radius + distance * otherNodeRadius - otherNodeRadius;
    }
  HrpHeader responseHeader (HrpHeader::SEARCH_RESPONSE, responsePayload, header.GetSearchId ());
  SendUnicast (responseHeader, destination);
}

void
HrpRoutingProtocol::SendEvaluate (HrpHeader header, Ipv4Address source)
{
  NS_LOG_FUNCTION (this << header << source);
  auto requestId = header.GetSearchId ();
  auto workingItem = m_searchesTable.Get (requestId);
  auto vsTable = workingItem.GetVsTable();

  auto responsePayload = header.GetPayload ();
  auto flip = this->m_rng->GetInteger (0, 1);
  if (flip)
    {
      for (auto i = 0; i < 13; i++)
        {
          auto tmp = responsePayload.at(i * 2 + 1);
          responsePayload.at(i * 2 + 1)= responsePayload.at(i * 2);
          responsePayload.at(i * 2) = tmp;
        }
    }
  HrpHeader responseHeader (HrpHeader::EVALUATE, responsePayload, requestId);
  auto destination = vsTable.right.at (source);
  SendUnicast (responseHeader, destination);
}

void
HrpRoutingProtocol::SendEvaluateResponse (HrpHeader header, Ipv4Address destination)
{
  NS_LOG_FUNCTION (this << header << destination);
  auto requestId = header.GetSearchId ();
  auto requestPayload = header.GetPayload ();
  double response = 1.0;
  auto region = m_regionsTable.find (requestId);
  if (requestPayload.at(region->second * 2) >= 0 && requestPayload.at(region->second * 2 + 1) >= 0){
        response = 0.0;
  }
  HrpHeader responseHeader (HrpHeader::EVALUATE_RESPONSE, {response}, requestId);
  SendUnicast (responseHeader, destination);
  m_regionsTable.erase (region);
}

void
HrpRoutingProtocol::UpdateRoutingTable (HrpHeader header, Ipv4Address destination)
{
  NS_LOG_FUNCTION (this << header << destination);
  auto requestId = header.GetSearchId ();
  auto workingItem = m_searchesTable.Get(requestId);
  auto target = m_searchesTable.GetPosition (requestId);
  auto vsTable = workingItem.GetVsTable();
  auto record = workingItem.GetPayloadTable();

  auto requestPayload = header.GetPayload ();
  Ipv4Address toRemove;
  Ipv4Address toInsert;
  if (requestPayload.at (0) == 1.0)
    {
      toRemove = destination;
      toInsert = vsTable.left.at (destination);
    }
  else
    {
      toInsert = destination;
      toRemove = vsTable.left.at (destination);
    }
  record.erase (toRemove);
  vsTable.left.erase (destination);
  workingItem.SetPayloadTable (record);
  workingItem.SetVsTable (vsTable);
  m_searchesTable.Set(requestId, workingItem);
  if (record.size () > 1)
    {
      SendSearch (requestId);
    }
  else
    {
      m_routingTable.AddEntry (toInsert, target);
      m_searchesTable.Remove(requestId);
      NS_LOG_INFO("Found a nearest node (" << toInsert << ") to target ("
                                            << target.x << ", " << target.y
                                            << ") at time " << Simulator::Now().As(Time::S));
    }
}

void
HrpRoutingProtocol::UpdateHabitat ()
{
  // Calculate foci and radius using random values expressed in meters.
  NS_LOG_FUNCTION (this);
  Vector myPos;
  Ptr<MobilityModel> MM = m_ipv4->GetObject<MobilityModel> ();
  myPos.x = MM->GetPosition ().x;
  myPos.y = MM->GetPosition ().y;
  auto f1 = myPos;
  auto f2 = myPos;

  auto randomDistance = m_rng->GetInteger (MIN_FOCUS_DISTANCE, MAX_FOCUS_DISTANCE);
  f1.x = f1.x - randomDistance;
  randomDistance = m_rng->GetInteger (MIN_FOCUS_DISTANCE, MAX_FOCUS_DISTANCE);
  f1.y = f1.y - randomDistance;
  m_habitat.SetF1 (f1);

  randomDistance = m_rng->GetInteger (MIN_FOCUS_DISTANCE, MAX_FOCUS_DISTANCE);
  f2.x = f2.x + randomDistance;
  randomDistance = m_rng->GetInteger (MIN_FOCUS_DISTANCE, MAX_FOCUS_DISTANCE);
  f2.y = f2.y + randomDistance;
  m_habitat.SetF2 (f2);

  randomDistance = m_rng->GetInteger (MIN_DISTANCE, MAX_DISTANCE);
  m_habitat.SetRadius (randomDistance);
  NS_LOG_INFO ("Position (" << myPos.x << ", " << myPos.y << ") Habitat [" << m_habitat << "]");
}

bool
HrpRoutingProtocol::IsInSearch (Ipv4Address destination)
{
  auto dstPos = m_locationService->GetPosition (destination);
  if (CalculateDistance (dstPos, m_locationService->GetInvalidPosition ()) == 0 &&
      m_locationService->IsInSearch (destination)){
      return true;
  }
  return m_searchesTable.Exist (dstPos);
}

}