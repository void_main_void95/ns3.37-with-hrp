/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*

 */
#ifndef HRP_H
#define HRP_H


#include "ns3/node.h"
#include "ns3/ipv4-routing-protocol.h"
#include "ns3/ipv4-interface.h"
#include "ns3/ipv4-l3-protocol.h"
#include "ns3/ip-l4-protocol.h"
#include "ns3/mobility-model.h"
#include "ns3/ipv4-header.h"
#include "ns3/ipv4-address.h"
#include "ns3/ipv4-route.h"
#include "ns3/location-service.h"
#include "ns3/god.h"

#include "hrp-request-queue.h"

#include "hrp-header.h"
#include "hrp-routing-table.h"
#include "hrp-habitat.h"
#include "hrp-searches-table.h"
#include <boost/bimap.hpp>

#include <map>
#include <complex>

#define MIN_FOCUS_DISTANCE 5
#define MAX_FOCUS_DISTANCE 20
#define MIN_DISTANCE 4
#define MAX_DISTANCE 10

namespace ns3::hrp {
/**
 * \ingroup HRP
 *
 * \brief HRP routing protocol
 */
class HrpRoutingProtocol : public Ipv4RoutingProtocol
{
public:
  static TypeId GetTypeId ();
  static const uint32_t HRP_PORT;

  /// c-tor
  HrpRoutingProtocol ();
  ~HrpRoutingProtocol () override;
  void DoDispose () override;


  ///\name From Ipv4RoutingProtocol
  //
  //
  Ptr<Ipv4Route> RouteOutput (Ptr<Packet> p, const Ipv4Header &header, Ptr<NetDevice> oif, Socket::SocketErrno &sockerr) override;
  bool RouteInput (Ptr<const Packet> p, const Ipv4Header &header, Ptr<const NetDevice> idev,
                   UnicastForwardCallback ucb, MulticastForwardCallback mcb,
                   LocalDeliverCallback lcb, ErrorCallback ecb) override;
  void NotifyInterfaceUp (uint32_t interface) override;
  int GetProtocolNumber () const;
  void NotifyInterfaceDown (uint32_t interface) override;
  void NotifyAddAddress (uint32_t interface, Ipv4InterfaceAddress address) override;
  void NotifyRemoveAddress (uint32_t interface, Ipv4InterfaceAddress address) override;
  void SetIpv4 (Ptr<Ipv4> ipv4) override;
  virtual void RecvHRP (Ptr<Socket> socket);
  virtual bool IsMyOwnAddress (Ipv4Address src);

  Ptr<Ipv4> m_ipv4;
  /// Raw socket per each IP interface, map socket -> iface address (IP + mask)
  typedef std::map< Ptr<Socket>, Ipv4InterfaceAddress> Sockets;
  typedef std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator SocketsCI;
  Sockets m_socketAddresses;
  /// Loopback device used to defer RREQ until packet will be fully formed
  Ptr<NetDevice> m_lo;

  Ptr<LocationService> GetLS ();
  void SetLS (Ptr<LocationService> locationService);

  void SetDownTarget (IpL4Protocol::DownTargetCallback callback);
  IpL4Protocol::DownTargetCallback GetDownTarget () const;

  void PrintRoutingTable (ns3::Ptr<ns3::OutputStreamWrapper> stream, Time::Unit unit) const override
  {
    m_routingTable.Print(*stream->GetStream());
  }

private:
  /// Start protocol operation
  void Start ();
  /// Queue packet and send route request
  void DeferredRouteOutput (Ptr<const Packet> p, const Ipv4Header & header, UnicastForwardCallback ucb, ErrorCallback ecb);
  /// If route exists and valid, forward packet.

  /// Queue packet and send route request
  Ptr<Ipv4Route> LoopbackRoute (const Ipv4Header & header, Ptr<NetDevice> oif);

  /// If route exists and valid, forward packet.
  bool Forwarding (Ptr<const Packet> p, const Ipv4Header & header, UnicastForwardCallback ucb, ErrorCallback ecb);

  /// Find socket with local interface address iface
  Ptr<Socket> FindSocketWithInterfaceAddress (Ipv4InterfaceAddress iface) const;

  //Check packet from deffered route output queue and send if position is already available
//returns true if the IP should be erased from the list (was sent/droped)
  bool SendPacketFromQueue (Ipv4Address dst);

  //Calls SendPacketFromQueue and re-schedules
  void CheckQueue ();

  Time AskForPresenceInterval;
  Timer AskForPresenceTimer;

  uint32_t MaxQueueLen;                  ///< The maximum number of packets that we allow a routing protocol to buffer.
  Time MaxQueueTime;                     ///< The maximum period of time that a routing protocol is allowed to buffer a packet for.
  HrpRequestQueue m_queue;

  Timer CheckQueueTimer;
  uint8_t LocationServiceName;

  std::list<Ipv4Address> m_queuedAddresses;
  Ptr<LocationService> m_locationService;

  IpL4Protocol::DownTargetCallback m_downTarget;

  // Added for habitat information system
  typedef std::map<Ipv4Address, Ptr<Socket>> DiscoveredNeighborsTable;
  typedef std::map<uint32_t, uint8_t> RegionsTable;

  DiscoveredNeighborsTable m_discoveredNeighbors;
  RegionsTable m_regionsTable;
  HrpRoutingTable m_routingTable;
  HrpSearchesTable m_searchesTable;
  Habitat m_habitat;

  Ptr<UniformRandomVariable> m_rng;

  void SendBroadcast(HrpHeader header);
  void SendUnicast (HrpHeader header, Ipv4Address destination);
  void SendAskForPresence();
  void SendBeaconFrame (Ipv4Address destination);
  void UpdateDiscoveredNeighbours (HrpHeader header, Ptr<Socket> socket, Ipv4Address address);
  void SendCompareMe (Vector target);
  void SendCompareMeResponse (HrpHeader header, Ipv4Address destination);
  void UpdateBetterNeighbors (HrpHeader header, Ipv4Address address);
  void SendSearch (uint32_t id);
  void SendSearchResponse (HrpHeader header, Ipv4Address destination);
  void SendEvaluate(HrpHeader header, Ipv4Address source);
  void SendEvaluateResponse(HrpHeader header, Ipv4Address destination);
  void UpdateRoutingTable(HrpHeader header, Ipv4Address destination);
  void UpdateHabitat();
  bool IsInSearch(Ipv4Address dst);
};
} // ns3::hrp
#endif /* HRP_H */
